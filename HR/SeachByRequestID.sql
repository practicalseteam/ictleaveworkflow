SELECT  LeaveRequest.id AS RequestId,
Position.positionname	AS Position,
 Staff.id AS StaffId, 
 Staff.staffcode AS StaffCode, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 Department.departmentname AS Department,
 
 LeaveType.typename AS LeaveType,
 LeaveRequest.leave_start_date AS StartDate,
 LeaveRequest.leave_end_date_ AS EndDate,
 LeaveRequest.leave_start_period AS StartPeriod,
 LeaveRequest.leave_end_period AS EndPeriod,
 LeaveRequest.reason AS Reason,
 LeaveRequest.contact_info AS Contact

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE `status` = 'APPROVED'
AND LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id 
AND LeaveRequest.id = 15 -- a specific RequestId