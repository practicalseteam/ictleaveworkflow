<html>
<head>
<title>Seach by Request ID</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
 <div class="container">
    <div class="page-header">
        <h2>รายละเอียดการลา <small>ตามรหัสการลา</small></h2>
    </div>     
    
<?php
 require_once('../db/db_connect.php');
	mysql_query("SET NAMES UTF8");
 $requestID = intval($_GET['id']);

	$sql ="SELECT  LeaveRequest.id AS RequestId,
 Position.positionname	AS Position,
 Staff.id AS StaffId, 
 Staff.staffcode AS StaffCode, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 Department.departmentname AS Department,

 LeaveType.typename AS LeaveType,
 LeaveRequest.duration AS Duration,
 LeaveRequest.leave_start_date AS StartDate,
 LeaveRequest.leave_end_date AS EndDate,
 LeaveRequest.leave_start_period AS StartPeriod,
 LeaveRequest.leave_end_period AS EndPeriod,
 LeaveRequest.reason AS Reason,
 LeaveRequest.contact_info AS Contact,
 LeaveRequest.status AS Status,
 LeaveRequest.submit_date AS Submit


FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id ";

	$sql .= "AND LeaveRequest.id = ". $requestID;

	$result = $con->query($sql);
          if($row = $result->fetch_assoc()) { // Start If 
?>
     <div class="row" style="margin-top: 40px">
        <table class="table">
            <tr>
                <th>รหัสการลา:</th><td><?=$row["RequestId"]; ?></td>
                <th>สถานะการลา:</th><td><?=$row["Status"]; ?></td>
                <th>ประเภทการลา:</th><td><?=$row["LeaveType"]; ?></td>
            </tr>  
            <tr>
                 <th>วันที่ยื่นใบลา:</th><td><?=$row["Submit"]; ?></td>
                <th>ผู้ขอลา:</th><td><?=$row["FirstName"]."&nbsp;&nbsp;&nbsp;&nbsp;".$row["LastName"]; ?></td>
                <th>จำนวนวันลา:</th><td><?=$row["Duration"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;วัน</td>
            </tr>    
        </table>    
     </div>    
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body" id="result">  
<?php
}// End If 
      $sql ="SELECT  LeaveRequest.id AS RequestId,
 Position.positionname  AS Position,
 Staff.id AS StaffId, 
 Staff.staffcode AS StaffCode, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 Department.departmentname AS Department,
 
 LeaveType.typename AS LeaveType,
 LeaveRequest.leave_start_date AS StartDate,
 LeaveRequest.leave_end_date AS EndDate,
 LeaveRequest.leave_start_period AS StartPeriod,
 LeaveRequest.leave_end_period AS EndPeriod,
 LeaveRequest.reason AS Reason,
 LeaveRequest.contact_info AS Contact,
 LeaveRequest.status AS Status,
 LeaveRequest.submit_date AS Submit,
LeaveRequest.remark AS Remark

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id ";

      $sql .= "AND LeaveRequest.id = ". $requestID;

     // echo $sql;

      $result = $con->query($sql);
 $msg = "<table class=\"table\">";
 $msg .=                 "<thead> ";
 $msg .= "<th>รหัสบุคคล</th>";
 $msg .= "<th>รหัสเข้างาน</th>";
 $msg .= "<th>ตำแหน่ง</th>";
 $msg .= "<th>สังกัดงาน</th>";
 $msg .= "<th>วันเริ่มต้นการลา</th>";
 $msg .= "<th>วันสิ้นสุดการลา</th>";
 $msg .= "<th>ช่วงเริ่มการลา</th>";
 $msg .= "<th>ช่วงสิ้นสุดการลา</th>";
 $msg .= "<th>เหตุผล</th>";
 $msg .= "<th>ติดต่อ</th>";
 $msg .= "<th>หมายเหตุ</th>";
 $msg .= "</tr></thead>";
 $msg .= "<tbody>";	
if ($result->num_rows > 0) {
                        // output data of each row
                        $num=1;
                       while($row = $result->fetch_assoc()) {
                          $leavePeriodMsg ="N/A ไม่ระบุ";
                        if (strcmp ($row["StartPeriod"], "1") == 0 || strcmp($row["EndPeriod"], "1") == 0){
                             $leavePeriodMsg ="ครึ่งวันเช้า";
                        }
        
                        if (strcmp ($row["StartPeriod"], "2") == 0 || strcmp($row["EndPeriod"], "2") == 0){
                             $leavePeriodMsg ="ครึ่งวันบ่าย";
                        }
                         if (strcmp ($row["StartPeriod"], "3") == 0 || strcmp($row["EndPeriod"], "3") == 0){
                             $leavePeriodMsg ="เต็มวัน";
                        }

                         $msg .="<tr>";
                         $msg .=     "<td>". $row["StaffId"] ."</td>";
                         $msg .=    "<td>". $row["StaffCode"] ."</td>";
                         $msg .=	"<td>". $row["Position"] ."</td>";
                         
                       
                         $msg .=	"<td>". $row["Department"] ."</td>";

                         $msg .=	"<td>". $row["StartDate"]."</td>";
                         $msg .=	"<td>". $row["EndDate"]."</td>";
                         $msg .=	"<td>".  $leavePeriodMsg."</td>";
                         $msg .=	"<td>". $leavePeriodMsg."</td>";
                         $msg .=	"<td>". $row["Reason"]."</td>";
                         $msg .=	"<td>". $row["Contact"]."</td>";
                         $msg .=    "<td>". $row["Remark"]."</td>";
                        
                         $msg .="</tr>";

                        $num++;
                        }
                    }
                     
 $msg .= " </tbody>";
 $msg .= "</table>";
 echo $msg;

?>
            </div>
        </div>
    </div>
 </div>   
</body>
</html> 