<html>
<head>
<title>Seach by Staff ID</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
 <div class="container">
    <div class="page-header">
        <h2>ประวัติการลา <small>ตามบุคคล</small></h2>
    </div> 

<?php
 require_once('../db/db_connect.php');
	mysql_query("SET NAMES UTF8");
 $RequestId = intval($_GET['id']);
 $sql ="SELECT Position.positionname  AS Position,
 Staff.id AS StaffId, 
 Staff.staffcode AS StaffCode, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 Department.departmentname AS Department,

 LeaveType.typename AS LeaveType,

 LeaveRequest.leave_start_date AS StartDate,
 LeaveRequest.leave_end_date AS EndDate,
 LeaveRequest.leave_start_period AS StartPeriod,
 LeaveRequest.leave_end_period AS EndPeriod,
 LeaveRequest.reason AS Reason,
 LeaveRequest.contact_info AS Contact

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id ";

    $sql .= "AND Staff.id=  ". $RequestId;

    $result = $con->query($sql); 
    if($row = $result->fetch_assoc()) { // Start If 
?>
     <div class="row" style="margin-top: 40px">
        <table class="table">
            <tr>
                <th>รหัสเข้างาน:</th><td><?=$row["StaffCode"]; ?></td>
                <th>ชื่อ:</th><td><?=$row["FirstName"]; ?></td>
                <th>สกุล:</th><td><?=$row["LastName"]; ?></td>
            </tr>  
            <tr>
                 <th>รหัสบุคคล:</th><td><?=$row["StaffId"]; ?></td>
                <th>ตำแหน่ง:</th><td><?=$row["Position"]; ?></td>
                <th>สังกัดงาน:</th><td><?=$row["Department"]; ?></td>
            </tr>    
        </table>    
     </div>    
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body" id="result">  

<?php   
}// End If 
 $sql ="SELECT  LeaveRequest.id AS RequestId,
 Position.positionname  AS Position,
 Staff.id AS StaffId, 
 Staff.staffcode AS StaffCode, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 Department.departmentname AS Department,
 
 LeaveType.typename AS LeaveType,
  LeaveRequest.duration AS Duration,
 LeaveRequest.leave_start_date AS StartDate,
 LeaveRequest.leave_end_date AS EndDate,
 LeaveRequest.leave_start_period AS StartPeriod,
 LeaveRequest.leave_end_period AS EndPeriod,
 LeaveRequest.reason AS Reason,
 LeaveRequest.contact_info AS Contact

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id ";

    $sql .= "AND Staff.id=  ". $RequestId;

    //echo $sql;

    $result = $con->query($sql); 
 $msg = "<table class=\"table\">";
 $msg .=                 "<thead> ";
 $msg .= "<th>#</th>";
 $msg .= "<th>รหัสการลา</th>";
  $msg .= "<th>จำนวนวันลา</th>";
 $msg .= "<th>ประเภทการลา</th>";
 $msg .= "<th>วันเริ่มต้นการลา</th>";
 $msg .= "<th>วันสิ้นสุดการลา</th>";
 $msg .= "<th>ช่วงเริ่มการลา</th>";
 $msg .= "<th>ช่วงสิ้นสุดการลา</th>";
 $msg .= "<th>เหตุผล</th>";
 $msg .= "<th>ติดต่อ</th>";
 $msg .= "</tr></thead>";
 $msg .= "<tbody>";	
if ($result->num_rows > 0) {
                        // output data of each row
                        $num=1;
                       
                       while($row = $result->fetch_assoc()) {
                         $leavePeriodMsg ="N/A ไม่ระบุ";
                        if (strcmp ($row["StartPeriod"], "1") == 0 || strcmp($row["EndPeriod"], "1") == 0){
                             $leavePeriodMsg ="ครึ่งวันเช้า";
                        }
        
                        if (strcmp ($row["StartPeriod"], "2") == 0 || strcmp($row["EndPeriod"], "2") == 0){
                             $leavePeriodMsg ="ครึ่งวันบ่าย";
                        }
                         if (strcmp ($row["StartPeriod"], "3") == 0 || strcmp($row["EndPeriod"], "3") == 0){
                             $leavePeriodMsg ="เต็มวัน";
                        }

                         $msg .="<tr>";
                         $msg .=   "<td>". $num ."</td>";
                         $msg .=	"<td>". $row["RequestId"] ."</td>";
                          $msg .=   "<td>". $row["Duration"] ."</td>";
                         $msg .=	"<td>". $row["LeaveType"] ."</td>";
                         $msg .=	"<td>". $row["StartDate"]."</td>";
                         $msg .=	"<td>". $row["EndDate"]."</td>";
                         $msg .=	"<td>".  $leavePeriodMsg."</td>";
                         $msg .=	"<td>".  $leavePeriodMsg."</td>";
                         $msg .=	"<td>". $row["Reason"]."</td>";
                         $msg .=	"<td>". $row["Contact"]."</td>";
                        
                         $msg .="</tr>";

                        $num++;
                        }
                    }
                     
 $msg .= " </tbody>";
 $msg .= "</table>";
 echo $msg;

?>
            </div>
        </div>
    </div>
 </div>   
</body>
</html> 