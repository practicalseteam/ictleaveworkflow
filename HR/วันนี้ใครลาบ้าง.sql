SELECT  LeaveRequest.id AS RequestId,
Position.positionname	AS Position,
 Staff.id AS StaffId, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 LeaveType.typename AS LeaveType,
 Department.departmentname AS Department,
 LeaveRequest.leave_end_date_ AS EndDate

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE `status` = 'APPROVED'

AND LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id
AND NOW() <= LeaveRequest.leave_end_date_
AND NOW() >= LeaveRequest.leave_start_period
ORDER BY RequestId