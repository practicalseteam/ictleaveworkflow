<!DOCTYPE html>
<?php
ob_start();
session_start();
ob_end_flush();
?>
<html>

 <script>
    function test()
    {
        var sort = document.getElementById("sort").value;
        var sortRule= document.getElementById("sortRule").value;
        var department = document.getElementById("department").value;
        var leveType = document.getElementById("leveType").value;
        var statusT = document.getElementById("statusT").value;

        var startDate = document.getElementById("startDate").value;
        var endDate = document.getElementById("endDate").value;

         var period = document.getElementById("period").value;


        var msg = "Sort:"+sort + "\n"
                    +"SortRule:"+sortRule + "\n"
                    +"Dertment"+department + "\n"
                    +"LeveType:"+leveType + "\n"
                     +"Status:"+statusT + "\n"
                    +"StartDate:"+startDate + "\n"
                    +"EndDate:"+endDate + "\n"
                    +"Period:"+period;
        //alert(msg);
        sendData(sort, sortRule, department, leveType, statusT, startDate, endDate, period);
    }
function sendData(sort, sortRule, department, leveType, statusT, startDate, endDate, period) {
   
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("result").innerHTML=xmlhttp.responseText;
    }
  }

  xmlhttp.open("GET","Search_Result.php?sort="+sort+"&sortRule="+sortRule+"&department="+department+"&leveType="+leveType+"&statusT="+statusT+"&startDate="+ startDate+"&endDate="+endDate+"&period="+period,true);
  xmlhttp.send();
}


</script>
<script>
function toPDF() {
        var sort = document.getElementById("sort").value;
        var sortRule= document.getElementById("sortRule").value;
        var department = document.getElementById("department").value;
        var leveType = document.getElementById("leveType").value;
        var statusT = document.getElementById("statusT").value;

        var startDate = document.getElementById("startDate").value;
        var endDate = document.getElementById("endDate").value;

         var period = document.getElementById("period").value;

    window.open("tcpdf/HR_PDF.php?sort="+sort+"&sortRule="+sortRule+"&department="+department+"&leveType="+leveType+"&statusT="+statusT+"&startDate="+ startDate+"&endDate="+endDate+"&period="+period);
  
}
</script>

<head>
    <?php
    require_once('header.php');
    ?>

</head>
<body onchange="test()">
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="page-header">
        <h2>ระบบการลางานออนไลน์ <small>ICT Leave Request and Approval System</small></h2>
    </div>

    <div class="row" style="margin-top: 40px">

        <div class="col-md-9"><h4>รายการลา</h4></div>
         <div class="col-md-2">
            <select class="form-control" id="sort">
                <option value="0" >เรียงด้วย: รหัสการลา</option>
                <option value="1" >ประเภทการลา</option>
                <option value="2" >ชื่อ</option>
                <option value="3" >สกุล</option>
                <option value="4" >สังกัดงาน</option>
                <option value="5" >ตำแหน่ง</option>
            </select>
        </div>
         <div class="col-md-1">
            <select class="form-control" id="sortRule">
                <option value="0" >น้อย->มาก</option>
                <option value="1" >มาก->น้อย</option>
            </select>
        </div>
         <div class="col-md-2">
            <select class="form-control" id="department">
                <option value="0" >ทุกสังกัดงาน</option>
                <option value="1" >บอร์ดผู้บริหาร</option>
                <option value="2" >บริหารและพัฒนาระบบ</option>
                <option value="3" >ทรัพยากรมนุษย์</option>
            </select>
        </div>
        <div class="col-md-2">
            <select class="form-control" id="leveType">
                <option value="0" >ทุกประเภทการลา</option>
                <option value="1" >ลาป่วย</option>
                <option value="2" >ลากิจ</option>
                <option value="3" >ลาพักผ่อน</option>
                <option value="4" >ลาพักคลอดบุตร</option>
                <option value="5" >ลาเลี้ยงดูบุตร</option>
            </select>
        </div>
         <div class="col-md-2">
            <select class="form-control" id="statusT">
                <option value="0" >สถานะขอลา: อนุมัติ</option>
                <option value="1" >อยู่ในระหว่างดำแนินการ</option>
                <option value="2" >เกินกำหนด</option>
                <option value="3" >ไม่อนุมัติ</option>
            </select>
        </div>
         <div class="col-md-2">
            วันที่เริ่มต้น<input type="date" id="startDate">
        </div>
         <div class="col-md-2">
            วันที่สิ้นสุด<input type="date" id="endDate">
        </div>
        <div class="col-md-2">
            <select class="form-control" id="period">
                <option value="0" >ตัวกรอง:วันนี้</option>
                <option value="1" >เมื่อวาน</option>
                <option value="2" >7วัน</option>
                <option value="3" >1 เดือน</option>
                 <option value="4" >3 เดือน</option>
                  <option value="5" >6 เดือน</option>
                   <option value="6" >9 เดือน</option>
                    <option value="7" >1 ปี</option>
            </select>
            <button onclick="toPDF()">Export to PDF</button>

        </div>
    <div class="row">

</div><!-- /.row -->
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body" id="result">

                <?php


                $sql= "SELECT  LeaveRequest.id AS RequestId,
Position.positionname   AS Position,
Staff.id AS StaffId,
 Staff.staffcode AS StaffCode,
 Staff.firstname AS FirstName,
 Staff.lastname AS LastName,
 LeaveType.typename AS LeaveType,
 Department.departmentname AS Department,
 LeaveRequest.leave_end_date AS EndDate

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE `status` = 'APPROVED'
AND LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id
AND  (LeaveRequest.leave_start_period <= NOW() AND LeaveRequest.leave_end_date >= NOW())
ORDER BY RequestId";
                    $result = $con->query($sql);
                    $totalLine= $result->num_rows;

                ?>
                <h5>สรุปจำนวนการลาวันนี้ที่ได้รับการอนุมัติจากทุกประเภทการลาและจากทุกสังกัดงานโดยเรียงด้วยรหัสการลาจากน้อยไปหามากตามตัวอักษร</h5>
                <h5>มีทั้งสิ้น  <? echo "  ".$totalLine; ?> รายการ</h5>
                <!-- Table -->
               <table class="table">
                 <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัสการลา</th>
                        <th>ตำแหน่ง</th>
                        <th>รหัสบุคล</th>
                        <th>รหัสเข้างาน</th>
                        <th>ชื่อ</th>
                        <th>สกุล</th>
                        <th>ประเภทการลา</th>
                        <th>สังกัดงาน</th>
                        <th>สิ้นสุดวันลา</th>

                    </tr>
                </thead>
                <tbody>
                     <?php

                       if ($result->num_rows > 0) {
                        // output data of each row
                        $num=1;
                       while($row = $result->fetch_assoc()) {
                         $requestIDLink ="<a href=\"HR/SearchByRequestID.php?id=". $row["RequestId"]."\" target=\"_blank\">". $row["RequestId"] ."</a>";
                         $staffIDLink ="<a href=\"HR/SearchByStaffID.php?id=". $row["StaffId"] ."\" target=\"_blank\">". $row["StaffId"] ."</a>";
                         $department = "'". $row["Department"]."'";
                         $departmentNameLink ="<a href=\"HR/SearchByDepartment.php?name=". $department."\" target=\"_blank\">". $row["Department"] ."</a>";
                         echo "<tr><td>" . $num ."</td><td>". $requestIDLink ."</td><td>". $row["Position"] ."</td>"."<td>". $staffIDLink. "</td>"."<td>". $row["StaffCode"] ."</td><td>". $row["FirstName"] ."</td><td>" . $row["LastName"] ."</td><td>". $row["LeaveType"] ."</td><td>".  $departmentNameLink ."</td><td>". $row["EndDate"]."</td></tr>";

                        $num++;
                        }
                    }
                     ?>

                 </tbody>
                </table>

            </div>
        </div>
    </div> <!-- End Row -->
</div>

<?php require_once('footer.php') ?>
</body>
</html>
