<?php


 require_once('db/db_connect.php');


$sort = intval($_GET['sort']);
$sortRule = intval($_GET['sortRule']);
$department = intval($_GET['department']);
$leveType = intval($_GET['leveType']);
$status = intval($_GET['statusT']);

$startDate = $_GET['startDate'];
$endDate = $_GET['endDate'];
$period = $_GET['period'];

$sql = "SELECT  LeaveRequest.id AS RequestId,
Position.positionname	AS Position,
 Staff.id AS StaffId, 
 Staff.staffcode AS StaffCode, 
 Staff.firstname AS FirstName, 
 Staff.lastname AS LastName,
 LeaveType.typename AS LeaveType,
 Department.departmentname AS Department,
 LeaveRequest.leave_start_date AS StartDate,
 LeaveRequest.leave_end_date AS EndDate

FROM LeaveRequest, Staff, Position, LeaveType, Department
WHERE LeaveRequest.leaverequest_owner_id = Staff.id
AND Staff.position_id = Position.id
AND LeaveRequest.LeaveType_id = LeaveType.id
AND Staff.department_id = Department.id ";
if( $status ==0){
	$sql .= " AND status = 'APPROVED' ";  
}
if( $status ==1){
	$sql .= " AND status = 'IN PROGRESS' ";  
}
if( $status ==2){
	$sql .= " AND status = 'OVERDUE' ";  
}
if( $status ==3){
	$sql .= " AND status = 'REJECT' ";  
}

if( $leveType ==1){
	$sql .= " AND LeaveType.id= 1 ";  
}
if( $leveType ==2){
	$sql .= " AND LeaveType.id= 2 ";  
}
if( $leveType ==3){
	$sql .= " AND LeaveType.id= 3 ";  

}
if( $leveType ==4){
	$sql .= " AND LeaveType.id= 4 ";  
	
}
if( $leveType ==5){
	$sql .= " AND LeaveType.id= 5 ";  
	
}
if( $department ==1){
	$sql .= " AND Department.id= 1 ";  
}
if( $department ==2){
	$sql .= " AND Department.id= 2 ";  
}
if( $department ==3){
	$sql .= " AND Department.id= 3 ";  
}
if($startDate =="" && $endDate==""  && $period==0){

	$sql .= " /*condition 1 */ ";	
	$sql .= " AND  (LeaveRequest.leave_start_period <= NOW() AND LeaveRequest.leave_end_date >= NOW()) ";
}
if($startDate!="" && $endDate=="" && $period==0){
	$date = date('Y-m-d');
	if($startDate <= $date){
		$sql .= " /*condition 2 */ ";	
        $sql .= " AND  (LeaveRequest.leave_start_period <= ".$startDate." AND LeaveRequest.leave_end_date >= NOW()) ";
     }else{
     	echo "<h5 style='color:red'>ช่วงวันที่ที่เลือกไม่มีความเป็นไปได้ ดังนั้นผลลัพธ์จึงผิดเพื้ยน </h3>";
     	echo "<h5 style='color:red'>The selected date period is not possible.Thus, the outcome may be incorrect. </h3>";
     
     }   
}
if($startDate!="" && $endDate!="" && $period==0){
	
	if($startDate <= $endDate){
		$sql .= " /*condition 3 */ ";	
        $sql .= " AND   (LeaveRequest.leave_start_period between ".$startDate." and ".$endDate.")
       						 OR (LeaveRequest.leave_end_date between ".$startDate." and ".$endDate.")
       							/* OR (LeaveRequest.leave_start_period <= ".$startDate." AND LeaveRequest.leave_end_date >= ". $endDate.") */";
     }else{
     	echo "<h5 style='color:red'>ช่วงวันที่ที่เลือกไม่มีความเป็นไปได้ ดังนั้นผลลัพธ์จึงผิดเพื้ยน </h3>";
     	echo "<h5 style='color:red'>The selected date period is not possible.Thus, the outcome may be incorrect. </h3>";
     }   
}
/*
1 = yester
2 = 1 week
3 = 1 months
4 = 3 months
5 = 6 months
6 = 9 year
7 = 1 Year
*/        
       
switch ($period) {
    case 1:
    	$sql .= " /* Case #1 Yester*/ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -1 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -1 day)) ";
        break;
    case 2:
       $sql .= " /* Case #2 1 Week */ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -7 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -7 day)) ";
        break;
    case 3:
        $sql .= " /* Case #3 1 Months */ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -30 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -30 day)) ";
        break;
     case 4:
        $sql .= " /* Case #4 3 Months */ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -90 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -90 day)) ";
        break;
    case 5:
        $sql .= " /* Case #5 6 Months */ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -180 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -180 day)) ";
        break;
    case 6:
        $sql .= " /* Case #6 9 Months  */ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -270 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -270 day)) ";
        break;
    case 7:
       $sql .= " /* Case #7 1 Year */ ";
       	$sql .= " AND  (LeaveRequest.leave_start_period <= DATE_ADD(CURDATE(), INTERVAL -365 day) AND LeaveRequest.leave_end_date >= DATE_ADD(CURDATE(), INTERVAL -365 day)) ";
        break;
}


if($sort ==0 ){
	$sql .= " ORDER BY RequestId ";
}
if($sort ==1 ){
	$sql .= " ORDER BY LeaveType ";
}
if($sort ==2 ){
	$sql .= " ORDER BY FirstName ";
}
if($sort ==3 ){
	$sql .= " ORDER BY LastName ";
}
if($sort ==4 ){
	$sql .= " ORDER BY Department ";
}
if($sort ==5 ){
	$sql .= " ORDER BY Position ";
}
if($sortRule ==1){
	$sql .="  DESC ";
}





//echo $sql;



 $result = $con->query($sql);
 $totalLine= $result->num_rows;
 /*
echo "<br><br>";
echo "Sort:".$sort;
echo "SortRule:" . $sortRule;
echo "Department:".$department;
echo "leveType:".$leveType;
echo "Status:". $status;
echo "startDate:".$startDate;
echo "endDate:".$endDate;
echo "Period:".$period;
*/

$recordMsg="<h5>สรุป";
if($startDate == "" && $endDate == ""){
switch ($period) {
	case 0:
        $recordMsg.= "จำนวนการลาวันนี้";
        break;
    case 1:
       $recordMsg.="จำนวนการลาเมื่อวาน";
        break;
    case 2:
        $recordMsg.="จำนวนการลาในช่วง 7 ที่ผ่านมา";
        break;
    case 3:
        $recordMsg.="จำนวนการลาในช่วง 1 เดือนที่ผ่านมา";
        break;
     case 4:
        $recordMsg.="จำนวนการลาในช่วง 3 เดือนที่ผ่านมา";
        break;
    case 5:
        $recordMsg.="จำนวนการลาในช่วง 6 เดือนที่ผ่านมา";
        break;
    case 6:
        $recordMsg.="จำนวนการลาในช่วง 9 เดือนที่ผ่านมา:";
        break;
    case 7:
        $recordMsg.="จำนวนการลาในช่วง 1 ปีที่ผ่านมา";
        break;
}
}else{
	 if($startDate != "" && $endDate == ""){
	 		$recordMsg.="จำนวนการลาในช่วงวันที่".$startDate."จนถึงวันนี้";
	 }
	 if($startDate =="" && $endDate != ""){
	 		echo "<h5 style='color:red'>กรุณาระบุวันที่เริ่มต้น</h5>";
	 		echo "<h5 style='color:red'>Please specify the start date period input.</h5>";
	 }
	 if($startDate != "" && $endDate != ""){
	 		$recordMsg.="จำนวนการชาในช่วงวันที่".$startDate."จนถึงวันที่".$endDate;
	 }
}
switch ($status) {
    case 1:
       $recordMsg.="ที่กำลังอยู่ในสถานะอยู่ระหว่างดำเนินการ";
        break;
    case 2:
        $recordMsg.="ที่อยู่ในสถานะเกินกำหนด";
        break;
    case 3:
        $recordMsg.="ที่ไม่ได้รับการอนุมัติ";
        break;
    default:
         $recordMsg.= "ที่ได้รับการอนุมัติ";
}
switch ($LeaveType) {
    case 1:
       $recordMsg.="จากประเภทลาป่วย";
        break;
    case 2:
        $recordMsg.="จากประเภทลากิจ";
        break;
    case 3:
        $recordMsg.="จากประเภทลาป่วย";
        break;
     case 4:
        $recordMsg.="จากประภทลาพักคลอดบุตร";
        break;
    case 5:
        $recordMsg.="จากประเภทลาเลี้ยงดูบุตร";
        break;
    default:
         $recordMsg.= "จากทุกประเภทการลา";
}
switch ($department) {
    case 1:
       $recordMsg.="และสังกัดงานบอร์ดผู้บริหาร";
        break;
    case 2:
        $recordMsg.="และจากสังกัดงานบริหารและพัฒนาระบบ";
        break;
    case 3:
        $recordMsg.="และจากสังกัดงานทรัพยากรมนุษย์";
        break;
    default:
         $recordMsg.= "และจากทุกสังกัดงาน";
}
switch ($sort) {
    case 1:
       $recordMsg.="โดยเรียงด้วยประเภทการลา";
        break;
    case 2:
        $recordMsg.="โดยเรียงด้วยชื่อ";
        break;
    case 3:
        $recordMsg.="โดยเรียงด้วยสกุล";
        break;
     case 4:
        $recordMsg.="โดยเรียงด้วยสังกัดงาน";
        break;
    case 5:
        $recordMsg.="โดยเรียงด้วยตำแหน่ง";
        break;
    default:
         $recordMsg.= "โดยเรียงด้วยรหัสการลา";
}

switch ($sortRule) {
    case 0:
       $recordMsg.="จากน้อยไปหามากตามตัวอักษร";
        break;
    case 1:
        $recordMsg.="จากมากไปหาน้อยตามตัวอักษร";
        break;
}
$recordMsg .= "</h5>";

 $msg = "";
 $msg .= $recordMsg;
 $msg .= "<h5>มีทั้งสิ้น  " .$totalLine ." รายการ</h5>";

 $msg .= "<table class=\"table\">";
 $msg .=                 "<thead> ";
 $msg .=  "<tr><th>#</th>";
 $msg .= "<th>รหัสการลา</th>";
 $msg .= "<th>ตำแหน่ง</th>";
 $msg .= "<th>รหัสบุคคล</th>";
 $msg .= "<th>รหัสเข้างาน</th>";
 $msg .= "<th>ชื่อ</th>";
 $msg .= "<th>สกุล</th>";
 $msg .= "<th>ประเภทการลา</th>";
 $msg .= "<th>แผนก</th>";
 $msg .= "<th>สิ้นสุดวันลา</th>";
 $msg .= "</tr></thead>";
 $msg .= "<tbody>";
if ($result->num_rows > 0) {
                        // output data of each row
                        $num=1;
                       while($row = $result->fetch_assoc()) {
                       	 $requestIDLink ="<a href=\"HR/SearchByRequestID.php?id=". $row["RequestId"]."\" target=\"_blank\">". $row["RequestId"] ."</a>";
                         $staffIDLink ="<a href=\"HR/SearchByStaffID.php?id=". $row["StaffId"] ."\" target=\"_blank\">". $row["StaffId"] ."</a>";
                         $department = "'". $row["Department"]."'";
                         $departmentNameLink ="<a href=\"HR/SearchByDepartment.php?name=". $department."\" target=\"_blank\">". $row["Department"] ."</a>";
                         $msg .="<tr>";
                         $msg .=  "<td>" . $num ."</td>";
                         $msg .=	"<td>".  $requestIDLink ."</td>";
                         $msg .=	"<td>". $row["Position"] ."</td>";
                         $msg .= 	"<td>".  $staffIDLink  ."</td>";
                         $msg .= 	"<td>". $row["StaffCode"] ."</td>";
                         $msg .=	"<td>". $row["FirstName"] ."</td>";
                         $msg .=	"<td>" . $row["LastName"] ."</td>";
                         $msg .=	"<td>". $row["LeaveType"] ."</td>";
                         $msg .=	"<td>".  $departmentNameLink ."</td>";
                         $msg .=	"<td>". $row["EndDate"]."</td>";
                         $msg .="</tr>";

                        $num++;
                        }
                    }
                     
 $msg .= " </tbody>";
 $msg .= "</table>";
 echo $msg;

?>