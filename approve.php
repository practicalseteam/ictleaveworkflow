<?php
ob_start();
session_start();
ob_end_flush();
require_once('db/db_connect.php');
$reqquery = mysqli_query($con,"SELECT LeaveRequest.id, firstname, lastname, positionname, typename, leave_start_date, leave_end_date, reason
							FROM ((LeaveRequest INNER JOIN Staff ON leaverequest_owner_id=Staff.id)
								INNER JOIN Position ON Staff.position_id=Position.id)
								INNER JOIN LeaveType ON LeaveRequest.LeaveType_id=LeaveType.id
							WHERE status = 'IN PROGRESS' 
							AND LeaveRequest.need_comment_by='".$_SESSION['role']."'
							ORDER BY submit_date ASC");
?>
<!DOCTYPE html>
<?php
require_once('header.php');
?>
</head>
<body>
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="page-header">
        <h2>ระบบการลางานออนไลน์ <small>ICT Leave Request and Approval System</small></h2>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <h3>สถิติการลางานวันนี้</h3>
        </div>
        <div class="col-lg-2 pull-right">
            <h3><a href="HR_Search.php" class="btn btn-primary btn-sm">ดูสถิติการลาทั้งหมด</a></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-2 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">ลาป่วย</div>
                    <div class="panel-body">
                        0 คน
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">ลากิจ</div>
                    <div class="panel-body">
                        0 คน
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">ลาพักผ่อน</div>
                    <div class="panel-body">
                        0 คน
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">ลาคลอด</div>
                    <div class="panel-body">
                        0 คน
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">ลาดูแลบุตรภรรยาฯ</div>
                    <div class="panel-body">
                        0 คน
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>อนุมัติ / แสดงความคิดเห็นในใบลา</h3>
        </div>
    </div>
    <?php
    // display error message when successfully approved
//    $cookie_name = "successapproved";
//    if(isset($_COOKIE[$cookie_name])){
//        echo "<div class='row'>";
//        echo "<div class='col-lg-12'>";
//        echo "<div class='alert alert-dismissable alert-success'>";
//        echo "<strong>ส่งความคิดเห็นเรียบร้อย</strong> ทางระบบได้เก็บความคิดเห็นของคุณต่อใบลานั้นๆ โปรดตรวจสอบใบลาที่เหลือ";
//        echo "</div></div></div>";
//        unset($_COOKIE[$cookie_name]);
//        $res = setcookie($cookie_name, '', time() - 3600);
//    } ?>
    <div class="row">
        <div class="col-lg-12">
            <?php while($row = mysqli_fetch_array($reqquery)): ?>
                <?php if(count($row)!=0): ?>
                    <form action="submitapproval.php?requestID=<?php echo $row['id'] ?>" method='post'>
                        <div class="well well-lg">
                            <div class="row">
                                <div class="col-lg-6" style="line-height: 2em">
                                    <strong>จาก </strong><?php echo $row['firstname']."&nbsp;&nbsp;".$row['lastname'] ?><br>
                                    <strong>ตำแหน่ง </strong><?php echo $row['positionname'] ?><br>
                                    <strong>ต้องการลา </strong><?php echo $row['typename'] ?><br>
                                    <strong>ตั้งแต่วันที่ </strong><?php echo $row['leave_start_date'].' <strong>ถึง</strong> '.$row['leave_end_date'] ?><br>
                                    <strong>เหตุผล </strong><?php echo $row['reason'] ?><br>
                                </div>
                                <div class="col-lg-6">
                                    <input type='radio' name='approve' value='0' checked>อนุมัติ
                                    <input type='radio' name='approve' value='1'>ไม่อนุมัติ<br>
                                    ข้อคิดเห็น :<br>
                                    <textarea name='comment' class='form-control' rows='2' wrap='on'></textarea><br>
                                    <input type='submit' class='btn btn-success' value='ตกลง'>
                                    <a href='approve.php' class='btn btn-default btn-sm'>ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </form>

                <?php else: ?>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            ไม่มีใบลาที่ต้องอนุมัติ
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php
echo "<table style='border-bottom:1px solid' width='1000' align='center'>";

while($row = mysqli_fetch_array($reqquery))
{
    if(count($row)!=0){
        echo "
					<form action='submitapproval.php?requestID=".$row['id']."' method='post'>
					<tr>
						<td>
							<table width='100%'>
								<tr>";
        echo "<td>จาก:&nbsp&nbsp".$row['firstname']."&nbsp&nbsp".$row['lastname']."</td>";
        echo "<td rowspan='5' width='500'>";
        echo "<input type='radio' name='approve' value='0' checked>อนุมัติ
										<input type='radio' name='approve' value='1'>ไม่อนุมัติ<br>
										ข้อคิดเห็น :<br>
										<textarea name='comment' rows='2' cols='40' wrap='on'></textarea><br>
										<input type='submit' value='ตกลง' style='width:80px; height:30px'>
										<a href='approve.php'><input type='button' value='ยกเลิก' style='width:80px; height:30px'></a>";
        echo "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>ตำแหน่ง:&nbsp&nbsp".$row['positionname']."</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>ประเภทการลา:&nbsp&nbsp".$row['typename']."</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>ลาตั้งแต่วันที่:&nbsp&nbsp".$row['leave_start_date']."</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>ถึงวันที่:&nbsp&nbsp".$row['leave_end_date_']."</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>เหตุผล:&nbsp&nbsp".$row['reason']."</td>";
        echo "</tr>";
        echo "</table>
						</td>
					</tr>
					</form>";
    }
    else{
        echo "ไม่มีใบลาที่ต้องอนุมัติ";
    }
}
echo "</table>";
?>
<?php
require_once('footer.php');
?>
</body>
</html>