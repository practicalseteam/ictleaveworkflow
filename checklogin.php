<?php
session_start();

require_once('db/db_connect.php');

// username and password sent from form 
$myusername=$_POST['myusername'];
$mypassword=$_POST['mypassword'];

//echo($myusername.'\n'.$mypassword);

$result = mysqli_query($con,"SELECT * FROM Staff AS s, Position AS p WHERE username='$myusername' and password='$mypassword' and s.position_id = p.id");
//echo($result);

// Mysql_num_row is counting table row
$count = mysqli_num_rows($result);
//echo($count);

// If result matched $myusername and $mypassword, table row must be 1 row
if($count==1){

// Register $myusername, $mypassword and redirect to file "login_success.php"

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $_SESSION['id'] = $row["id"];
    $_SESSION['name'] = $row["firstname"]." ".$row["lastname"];

    if($row["positionname"] == "HR Admin"){
        $_SESSION['role'] = "Admin";
    } elseif($row["positionname"] == "Dean"){
        $_SESSION['role'] = "DEAN";
    } elseif($row["positionname"] == "Deputy Dean"){
		$_SESSION['role'] = "DEPUTY DEAN";
	} elseif($row["positionname"] == "Supervisor"){
		$_SESSION['role'] = "SUPERVISOR";
	}
	else{
        $_SESSION['role'] = "general";
    }

    if($_SESSION['role']=="Admin"){
        header("Location: HR_Search.php");
    } elseif($_SESSION['role']=="DEAN" || $_SESSION['role']=="DEPUTY DEAN" || $_SESSION['role']=="SUPERVISOR"){
        header("Location: approve.php");
    } else{
        header("Location: userleavestatus.php");
    }
}
else {
    setcookie("failedlogin","Wrong Username or Password");
    header("Location: index.php");
}

?>

