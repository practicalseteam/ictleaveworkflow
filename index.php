<?php
ob_start();
session_start();
ob_end_flush();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once('header.php');

    //SQL query on the page
    //    $testresult = mysqli_query($con,"SELECT count(*) FROM Staff");

    ?>
</head>
<body>
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form role="form" method="post" action="checklogin.php">
                <h3>กรุณาเข้าสู่ระบบ</h3>
                <?php
                // display error message when login failed
                $cookie_name = "failedlogin";
                if(isset($_COOKIE[$cookie_name])){
                    echo"<p class='bg-danger'>".$_COOKIE[$cookie_name]."</p>";
                    unset($_COOKIE[$cookie_name]);
                    $res = setcookie($cookie_name, '', time() - 3600);
                }
                ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input name="myusername" type="text" class="form-control" id="staffUsername" placeholder="Enter username">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input name="mypassword" type="password" class="form-control" id="enterPassword" placeholder="Password">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Remember Me
                    </label>
                </div>
                <button type="submit" class="btn btn-default">Log in</button>
            </form>
        </div>
    </div>

    <!--    Test Database Connection-->
    <!--    <div class="panel panel-default">-->
    <!--        <div class="panel-heading">-->
    <!--            <h3 class="panel-title">Test SQL Query</h3>-->
    <!--        </div>-->
    <!--        <div class="panel-body">-->
    <!--            --><?php //echo mysqli_fetch_array($testresult)[0] ?><!-- staffs-->
    <!--        </div>-->
    <!--    </div>-->

</div>


<?php require_once('footer.php') ?>
</body>
</html>