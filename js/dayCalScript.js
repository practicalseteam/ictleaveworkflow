
function calcDays(d1, d2) {
	d1 = new Date(d1);
	d2 = new Date(d2);
	var diffDays = 0;
	if(d2.value != ""){
		while (d1 < d2) {
        	if (!(d1.getDay() == 0 || d1.getDay() == 6)) {
        		diffDays++;
        	}
        	d1.setDate(d1.getDate()+1);
        }
	}
	return diffDays;
}

function calcsDays(d1, d2) {
	d1 = new Date(d1);
	d2 = new Date(d2);
	var diffDays = 0;
	if(d2.value != "" && d2 > d1){
		while (d1 < d2) {
        	if (!(d1.getDay() == 0 || d1.getDay() == 6)) {
        		diffDays++;
        	}
        	d1.setDate(d1.getDate()+1);
        }
	}
	if (d2.value != "" && d2 < d1) {
   		while (d2 < d1) {
   			if (!(d2.getDay() == 0 || d2.getDay() == 6)) {
		        diffDays++;   
	    	}
	    	d2.setDate(d2.getDate()+1);
	    }
	}
	return diffDays;
}

function calcAllDays(d1, d2) { //include sat & sun
	d1 = new Date(d1);
	d2 = new Date(d2);
	var diffDays = 0;
	if(d2.value != "" && d2 > d1){
		while (d1 < d2) {
        	diffDays++;
        	d1.setDate(d1.getDate()+1);
        }

	}
	return diffDays;
}

function checkAllowDay(){
	var d1 = new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate();
	var d2 = document.getElementsByName("leave_start_date")[0].value;
	var day = calcDays(d1, d2);
	if (day <= 3 && !document.getElementsByName("chkEmer")[0].checked) {
		alert("วันที่เริ่มลาพักผ่อนต้องหลังจากวันที่ยื่นขออย่างน้อย 3 วันทำการ หรือเลือก \"ฉุกเฉิน\"");
		document.getElementsByName("leave_start_date")[0].value = "";

	}
}

function checkAllowSickDay(){
	var d1 = new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate();
	var d2 = document.getElementsByName("leave_start_date")[0].value;
	var day = calcsDays(d1, d2);
	if (day > 3 && !document.getElementsByName("chkEmer")[0].checked) {
		alert("ลาป่วยย้อนหลังได้ไม่เกิน 3 วันทำการ หรือเลือก \"ฉุกเฉิน\"");
		document.getElementsByName("leave_start_date")[0].value = "";
	}
}

function isMoreThreeDays() {
	if (document.getElementsByName("duration")[0].value >= 3)
		document.getElementById('attach').style.display = 'block';
	else
		document.getElementById('attach').style.display = 'none';
}

function calLeaveDuration() { //ลาพักผ่อน
	var d1 = document.getElementsByName("leave_start_date")[0].value;
	var d2 = document.getElementsByName("leave_end_date")[0].value;
	var lsp = getRVBN("leave_start_period");
	var lep = getRVBN("leave_end_period");
	var day = calcDays(d1, d2);

	ds = new Date(d1);
	de = new Date(d2);

	if (document.getElementsByName("totalLeaveDay")[0].value >= document.getElementsByName("totalAccumulatedVacationTime")[0].value) {
		alert("คุณหมดสิทธิ์ลาพักร้อนแล้ว");
		window.location.assign("userleavehistory.php");
	}
	if (ds.getDay() == 0 || ds.getDay() == 6) {
		alert("กรุณาเลือกวันทำการเท่านั้น");
		document.getElementsByName("leave_start_date")[0].value = "";
		d1 = "";
		day = 0;
	}
	else if (de.getDay() == 0 || de.getDay() == 6) {
		alert("กรุณาเลือกวันทำการเท่านั้น");
		document.getElementsByName("leave_end_date")[0].value = "";
		d2 = "";
		day = 0;
	}
	if (d1 != "" && d2 != "" && lsp != "" && lep != "") {
		if (d1 < d2) {
			if (lsp == lep)
				day += 0.5;
			else if (lsp == 1 && lep == 2)
				day++;
		}
		else if (d1 == d2) {
			if (lsp == lep)
				day = 0.5;
			else if (lsp == 1 && lep == 2)
				day = 1;
			else if (lsp == 2 && lep == 1) {
				alert("กรุณาเลือกเวลาใหม่");
				day = 0;
				document.getElementsByName("leave_end_date")[0].value = "";
			}
		}
		else if (d2 < d1){
			alert("กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
			document.getElementsByName("leave_end_date")[0].value = "";
		}

		document.getElementsByName("duration")[0].value = day;
	}
	if (day == 0)
		document.getElementsByName("duration")[0].value = "";
	if (day > 10) {
		alert("วันลาพักผ่อนติดต่อกันต้องไม่เกิน 10 วัน");
		document.getElementsByName("leave_end_date")[0].value = "";
		document.getElementsByName("duration")[0].value = "";
	}
	if (day > document.getElementsByName("totalAccumulatedVacationTime")[0].value - document.getElementsByName("totalLeaveDay")[0].value) {
		alert("จำนวนวันลาเกินกำหนด กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
		document.getElementsByName("leave_end_date")[0].value = "";
		document.getElementsByName("duration")[0].value = "";
	}

}

function calsickLeaveDuration() { //ลาป่วย
	var d1 = document.getElementsByName("leave_start_date")[0].value;
	var d2 = document.getElementsByName("leave_end_date")[0].value;
	var lsp = getRVBN("leave_start_period");
	var lep = getRVBN("leave_end_period");
	var day = calcDays(d1, d2);

	ds = new Date(d1);
	de = new Date(d2);

	if (document.getElementsByName("totalLeaveDay")[0].value >= 60) {
		alert("คุณหมดสิทธิ์ลาป่วยแล้ว");
		window.location.assign("userleavehistory.php");
	}
	if (ds.getDay() == 0 || ds.getDay() == 6) {
		alert("กรุณาเลือกวันทำการเท่านั้น");
		document.getElementsByName("leave_start_date")[0].value = "";
		d1 = "";
		day = 0;
	}
	else if (de.getDay() == 0 || de.getDay() == 6) {
		alert("กรุณาเลือกวันทำการเท่านั้น");
		document.getElementsByName("leave_end_date")[0].value = "";
		d2 = "";
		day = 0;
	}
	if (d1 != "" && d2 != "" && lsp != "" && lep != "") {
		if (d1 < d2) {
			if (lsp == lep)
				day += 0.5;
			else if (lsp == 1 && lep == 2)
				day++;
		}
		else if (d1 == d2) {
			if (lsp == lep)
				day = 0.5;
			else if (lsp == 1 && lep == 2)
				day = 1;
			else if (lsp == 2 && lep == 1) {
				alert("กรุณาเลือกเวลาใหม่");
				day = 0;
				document.getElementsByName("leave_end_date")[0].value = "";
			}
		}
		else if (d2 < d1){
			alert("กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
			document.getElementsByName("leave_end_date")[0].value = "";
		}

		document.getElementsByName("duration")[0].value = day;
	}
	if (day == 0)
		document.getElementsByName("duration")[0].value = "";
	if (day > 60 - document.getElementsByName("totalLeaveDay")[0].value) {
		alert("จำนวนวันลาเกินกำหนด กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
		document.getElementsByName("leave_end_date")[0].value = "";
		document.getElementsByName("duration")[0].value = "";
	}
	isMoreThreeDays();
}

function calBusinessDuration() { //ลากิจ
	var d1 = document.getElementsByName("leave_start_date")[0].value;
	var d2 = document.getElementsByName("leave_end_date")[0].value;
	var lsp = getRVBN("leave_start_period");
	var lep = getRVBN("leave_end_period");
	var day = calcDays(d1, d2);

	ds = new Date(d1);
	de = new Date(d2);

	if (document.getElementsByName("totalLeaveDay")[0].value >= 45) {
		alert("คุณหมดสิทธิ์ลากิจแล้ว");
		window.location.assign("userleavehistory.php");
	}
	if (ds.getDay() == 0 || ds.getDay() == 6) {
		alert("กรุณาเลือกวันทำการเท่านั้น");
		document.getElementsByName("leave_start_date")[0].value = "";
		d1 = "";
		day = 0;
	}
	else if (de.getDay() == 0 || de.getDay() == 6) {
		alert("กรุณาเลือกวันทำการเท่านั้น");
		document.getElementsByName("leave_end_date")[0].value = "";
		d2 = "";
		day = 0;
	}
	if (d1 != "" && d2 != "" && lsp != "" && lep != "") {
		if (d1 < d2) {
			if (lsp == lep)
				day += 0.5;
			else if (lsp == 1 && lep == 2)
				day++;
		}
		else if (d1 == d2) {
			if (lsp == lep)
				day = 0.5;
			else if (lsp == 1 && lep == 2)
				day = 1;
			else if (lsp == 2 && lep == 1) {
				alert("กรุณาเลือกเวลาใหม่");
				day = 0;
				document.getElementsByName("leave_end_date")[0].value = "";
			}
		}
		else if (d2 < d1){
			alert("กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
			document.getElementsByName("leave_end_date")[0].value = "";
		}

		document.getElementsByName("duration")[0].value = day;
	}
	if (day == 0)
		document.getElementsByName("duration")[0].value = "";
	if (day > 45 - document.getElementsByName("totalLeaveDay")[0].value) {
		alert("จำนวนวันลาเกินกำหนด กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
		document.getElementsByName("leave_end_date")[0].value = "";
		document.getElementsByName("duration")[0].value = "";
	}
}

function calmaternityDuration() {
	var d1 = document.getElementsByName("leave_start_date")[0].value;
	var d2 = document.getElementsByName("leave_end_date")[0].value;
	var day = 0;

	if (d1 != "" && d2 != "") {
		day = calcAllDays(d1, d2) + 1;
		if (d2 < d1){
			alert("กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
			document.getElementsByName("leave_end_date")[0].value = "";
		}
		if (day > 90) {
			alert("จำนวนวันลาเกินกำหนด");
			day = 0;
			document.getElementsByName("leave_end_date")[0].value = "";
		}
	}
	if (day == 0)
		document.getElementsByName("duration")[0].value = "";
	else
		document.getElementsByName("duration")[0].value = day;
}

function calParternityDuration() {
	var d1 = document.getElementsByName("leave_start_date")[0].value;
	var d2 = document.getElementsByName("leave_end_date")[0].value;
	var c = document.getElementsByName("child_dob")[0].value;
	var day = 0;

	if (calcAllDays(c, d1) + 1 > 90 || calcAllDays(c, d2) + 1 > 90) {
		alert("วันลาดูแลบุตรและภรรยาหลังคลอดต้องอยู่ในช่วง 90 วันนับจากวันเกินของลูก");
		document.getElementsByName("leave_start_date")[0].value = "";
		document.getElementsByName("leave_end_date")[0].value = "";
	}

	if (document.getElementsByName("totalLeaveDay")[0].value >= 15) {
		alert("คุณหมดสิทธิ์ลาดูแลบุตรและภารยาแล้ว");
		window.location.assign("userleavehistory.php");
	}
	if (d1 != "" && d2 != "") {
		day = calcAllDays(d1, d2) + 1;
		if (d2 < d1){
			alert("กรุณาเลือกวันลาวันสุดท้ายอีกครั้ง");
			document.getElementsByName("leave_end_date")[0].value = "";
		}
		if (day > 15 - document.getElementsByName("totalLeaveDay")[0].value) {
			alert("จำนวนวันลาเกินกำหนดจากที่มีอยู่");
			day = 0;
			document.getElementsByName("leave_end_date")[0].value = "";
		}
	}
	if (day == 0)
		document.getElementsByName("duration")[0].value = "";
	else
		document.getElementsByName("duration")[0].value = day;
}

function getRVBN(rName) {
    var radioButtons = document.getElementsByName(rName);
    for (var i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) return radioButtons[i].value;
    }
    return '';
}