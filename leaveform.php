<?php
ob_start();
session_start();
ob_end_flush();
require_once('db/db_connect.php');
$sqlStaff = "SELECT * FROM Staff WHERE id =". $_SESSION['id'];
$resultStaff = mysqli_query($con, $sqlStaff);
$resultStaff = mysqli_fetch_array($resultStaff);

$sqlPosition = "SELECT * FROM Position WHERE id =". $resultStaff['position_id'];
$resultPosition = mysqli_query($con, $sqlPosition);
$resultPosition = mysqli_fetch_array($resultPosition);

$sqlDepartment = "SELECT * FROM Department WHERE id =". $resultStaff['department_id'];
$resultDepartment = mysqli_query($con, $sqlDepartment);
$resultDepartment = mysqli_fetch_array($resultDepartment);

$sqlLeaveRequest = "SELECT leave_start_date, leave_end_date, duration, LeaveType_id FROM LeaveRequest WHERE leaverequest_owner_id = ". $_SESSION["id"]." AND status = \"APPROVED\" ORDER BY id DESC LIMIT 1";
$resultLeaveRequest = mysqli_query($con, $sqlLeaveRequest);
if ($resultLeaveRequest->num_rows > 0) {
    $resultLeaveRequest = mysqli_fetch_array($resultLeaveRequest);
    if ($resultLeaveRequest["LeaveType_id"] == 1) 
        $leaveString = "ลาป่วย";
    else if ($resultLeaveRequest["LeaveType_id"] == 4) 
        $leaveString = "ลาคลอดบุตร";
    else if ($resultLeaveRequest["LeaveType_id"] == 2) 
        $leaveString = "ลากิจส่วนตัว";
    else if ($resultLeaveRequest["LeaveType_id"] == 3) 
        $leaveString = "ลาพักผ่อน";
    else if ($resultLeaveRequest["LeaveType_id"] == 5) 
        $leaveString = "ลาดูแลบุตร";
     $startDate = $resultLeaveRequest["leave_start_date"];
     $endDate = $resultLeaveRequest["leave_end_date"];
     $duration = $resultLeaveRequest["duration"];
}
else {
     $leaveString = "-";
     $startDate = "-";
     $endDate = "-";
     $duration = "-";
}

//ปีงบ 1 ตค - 31 กย
if (date('m') >= 1 && date('m') <= 8)
    $year = date('Y')+1;
else
     $year = date('Y');
$sqlLeave = "SELECT * FROM LeaveRequest WHERE leaverequest_owner_id =". $_SESSION['id'] . " AND LeaveType_id = 1 AND status = \"APPROVED\" AND status = \"APPROVED\" AND submit_date BETWEEN \"".$year."-09-01\" AND \"".($year+1)."-08-31\"";
$resultLeave = mysqli_query($con, $sqlLeave);
$useDays = 0;
while ($row = $resultLeave->fetch_assoc()) {
    $useDays += $row["duration"];
}
?>

<script src="js/dayCalScript.js"></script>
<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["filePic"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["filePic"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["filePic"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["filePic"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["filePic"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>

<?
if(isset($_POST['submit']))
{
$imageName = mysql_real_escape_string($_FILES["filePic"]["name"]);
$imageDate = mysql_real_escape_string(file_get_contents($_FILES["filePic"]["tmp_name"]));
$imageType = mysql_real_escape_string($_FILES["filePic"]["type"]);

if(substr($imageType,0,5)=="filePic")
{
	echo "working code";
}
else{
echo "Not working code";
}
}
?>

<script src="js/dayCalScript.js"></script>
<html>
<head>
    <?php
    require_once('header.php');
    ?>
</head>
<body onload = "calsickLeaveDuration();">
<?php require_once('navbar.php') ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>แบบใบลาป่วย</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <div class="well bs-component" id="form_container">
                <form class="form-horizontal" name="form_data2" method="POST" action="submitform.php">
                    <fieldset>
                        <legend>กรุณากรอกข้อมูลให้ครบถ้วนก่อนยืนยันการส่งใบลา</legend>
                        <div class="form-group">
                            <label for="inputCodeEntry" class="col-lg-2 col-lg-offset-7 control-label">รหัสเข้างาน</label>
                            <div class="col-lg-3">
                               <input type="number" class="form-control" name="staffcode" value="<?php echo $resultStaff['staffcode']?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-2 col-lg-offset-7 control-label">วันที่</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" value="<?php echo date('d'); ?> / <?php echo date('m');?> / <?php echo (date('Y')+543); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-lg-2 control-label">เรื่อง</label>
                            <div class="col-lg-3">
							<input class="form-control" type="text" value="ขอลาป่วย" disabled>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" name="typename" value = "1">

                        <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label">ข้าพเจ้า ชื่อ</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" value="<?php echo $resultStaff['firstname']?>" disabled>
                            </div>
                            <label for="inputName" class="col-lg-2 control-label">นามสกุล</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" value="<?php echo $resultStaff['lastname']?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label">ตำแหน่ง</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" value="<?php echo $resultPosition['positionname']?>" disabled>
                            </div>
                            <label for="inputName" class="col-lg-2 control-label"> สังกัดงาน</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" value="<?php echo $resultPosition['positionname']?>" disabled>
                            </div>
                        </div>
							
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">เนื่องจาก</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" rows="5" name="reason" id="textReason" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label"> ได้ลาป่วยไปแล้ว </label>
                            <div class="col-lg-2">
                                <input class="form-control" name="totalLeaveDay" type="number" maxlength="" size="" value="<?php echo $useDays;?>" disabled>
                            </div>
                            <label for="inputDate" class="col-lg-0 control-label">วันทำการ</label>
                        </div>
                        <div class="form-group">
                            <label for="inputStartDate" class="col-lg-2 control-label">ขอลาป่วยตั้งแต่วันที่</label>
                            <div class="col-lg-2">
                                <input type="date" class="form-control" name="leave_start_date" onchange = "checkAllowSickDay();calsickLeaveDuration();" required>
                            </div>
                            <label for="input_start_period" class="col-lg-1 control-label"> ช่วง:</label>
                            <div class="col-lg-2">
                                <label class="radio-inline"><input type="radio" name="leave_start_period" value="1" required onchange = "calsickLeaveDuration();">เช้า </label>
                                <label class="radio-inline"><input type="radio" name="leave_start_period" value="2" required onchange = "calsickLeaveDuration();">บ่าย </label>
                            </div>
                            <div class="col-lg-2">
                                <label class="radio-inline"> <input type="checkbox" name="chkEmer" value="Emer" onchange = "calBusinessDuration();"> ฉุกเฉิน </label>    
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="inputEndDate" class="col-lg-2 control-label">ถึงวันที่</label>
                            <div class="col-lg-2">
                                <input type="date" class="form-control" name="leave_end_date" required onchange = "calsickLeaveDuration();">
                            </div>
                            <label for="inputPeriodLeave" class="col-lg-1 control-label"> ช่วง:</label>
                            <div class="col-lg-2">
                                <label class="radio-inline"><input type="radio" name="leave_end_period" value="1" required onchange = "calsickLeaveDuration();">เช้า </label>
                                <label class="radio-inline"><input type="radio" name="leave_end_period" value="2" required onchange = "calsickLeaveDuration();">บ่าย </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-2 control-label">มีกำหนด</label>
                            <div class="col-lg-1">
                                <input type = "text" class="form-control" name="duration" value = "" required readonly>
                            </div>
                            <label for="inputDate" class="col-lg-0 control-label">วัน</label>
                        </div>



                        <div class="form-group">
                            <label for="inputDate" class="col-lg-3 control-label">ข้าพเจ้าได้ลาครั้งสุดท้ายประเภท</label>
                            <div class="col-lg-3">
                                <input class="form-control" id="select" name="lasttypename" required value = "<?php echo $leaveString;?>" disabled><!--nameควรใช้เป็นอะไรดี   -->
                                   
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-2 control-label">ครั้งสุดท้ายตั้งแต่วันที่</label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" name="lastStartLeaveDate" required value = "<?php echo $startDate;?>" disabled>
                            </div>
                            <label for="inputDate" class="col-lg-2 control-label">ครั้งสุดท้ายถึงวันที่</label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" name="lastEndLeaveDate" required value = "<?php echo $endDate;?>" disabled>
                            </div>
                            <label for="inputDate" class="col-lg-1 control-label">มีกำหนด</label>
                            <div class="col-lg-1">
                                <input type="text" class="form-control" required value = "<?php echo $duration;?>" disabled>
                            </div>
                            <div class="col-lg-1">
                                <div class="control-label pull-left">วัน</div>
                            </div>
                        </div>
						
						<div class="form-group" id="attach" style="display: none">
						<label for="inputPic" class="col-lg-5 control-label" required>กรุณาแนบใบรับรองแพทย์</label>
						<div class="col-lg-3">
						<input type="file" class="form-control" name="filePic" id="filePic">
						</div>


						</div>
						<!--https://www.youtube.com/watch?v=kPGxWaIhLmk&index=1&list=PLqscjOOjS2nYOeZkJPXT8anlmd8KRmJVb   -->
                        <div class="form-group">
                            <label for="textArea" class="col-lg-3 control-label">ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" rows="5" name="contact_info" id="textAddress" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-default">Cancel</button>
                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php') ?>
</body>
</html>