<div class="bs-component">
    <div class="list-group">
        <a href="#" class="list-group-item active">
            ส่งใบลาออนไลน์
        </a>
        <a href="leaveform.php" class="list-group-item">ใบลาป่วย
        </a>
        <a href="bussinessleave.php" class="list-group-item">ใบลากิจ
        </a>
        <a href="vacationform.php" class="list-group-item">ใบลาพักผ่อน
        </a>
        <a href="maternityleave.php" class="list-group-item">ใบลาคลอด
        </a>
        <a href="paternityleave.php" class="list-group-item">ใบลาดูแลบุตรและภรรยาหลังคลอด
        </a>
    </div>
</div>
<div class="bs-component">
    <a href="userleavestatus.php" class="btn btn-primary btn-lg btn-block" style="margin-bottom: 20px">ดูสถานะใบลา</a>
</div>
<div class="bs-component">
    <a href="userleavehistory.php" class="btn btn-primary btn-lg btn-block">ประวัติการลาทั้งหมด</a>
</div>