<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ICT Leave Request and Approval System</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-collapse collapse navbar-inverse-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['role'])){
                    if(($_SESSION['role'])=='Admin'){
                        echo"<li><a href='HR_Search.php'>Leave Report</a></li>";
                    }
                    elseif($_SESSION['role']=="DEAN" || $_SESSION['role']=="DEPUTY DEAN" || $_SESSION['role']=="SUPERVISOR"){
                        echo"<li><a href='approve.php'>All Leave Requests</a></li>";
                        echo"<li><a href='HR_Search.php'>Leave Report</a></li>";
                    }
                }
                ?>
                <li class="active"><a href="newrequest.php">New Leave Request</a></li>
                <li class="dropdown">
                    <?php if(isset($_SESSION['name'])){ ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <?php echo $_SESSION['name']; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="userleavestatus.php">ดูสถานะใบลา</a></li>
                        <li><a href="userleavehistory.php">ประวัติการลา</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php">Log out</a></li>
                    </ul>
                    <?php } else{ ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, Guest <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="index.php">Log in</a></li>
                        </ul>
                    <?php } ?>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>