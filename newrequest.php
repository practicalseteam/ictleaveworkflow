<?php
ob_start();
session_start();
ob_end_flush();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once('header.php');

    ?>
</head>
<body>
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="page-header">
        <h2>ระบบการลางานออนไลน์ <small>ICT Leave Request and Approval System</small></h2>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>เลือกประเภทของใบลาที่ต้องการ</h3>
            <div class="row">
                <div class="col-lg-12">
                    <div class="list-group">
                        <a href="leaveform.php" class="list-group-item">
                            <h4 class="list-group-item-heading">ใบลาป่วย</h4>
                            <p class="list-group-item-text">ส่งหลังวันลาภายใน 3 วันทำการ สามารถลาได้ไม่เกิน 60 วันต่อปี</p>
                        </a>
                        <a href="bussinessleave.php" class="list-group-item">
                            <h4 class="list-group-item-heading">ใบลากิจธุระ</h4>
                            <p class="list-group-item-text">ส่งก่อนวันลาภายใน 3 วันทำการ สามารถลาได้ไม่เกิน 45 วันต่อปี</p>
                        </a>
                        <a href="vacationform.php" class="list-group-item">
                            <h4 class="list-group-item-heading">ใบลาพักผ่อน</h4>
                            <p class="list-group-item-text">ส่งก่อนวันลาภายใน 3 วันทำการ สามารถลาได้ไม่เกิน 10 วันต่อปี</p>
                        </a>
                        <a href="maternityleave.php" class="list-group-item">
                            <h4 class="list-group-item-heading">ใบลาคลอดบุตร</h4>
                            <p class="list-group-item-text">สำหรับบุคลากรที่มีครรภ์ สามารถลาได้ไม่เกิน 90 วันต่อปี</p>
                        </a>
                        <a href="paternityleave.php" class="list-group-item">
                            <h4 class="list-group-item-heading">ใบลาดูแลบุตรและภรรยาหลังคลอด</h4>
                            <p class="list-group-item-text">ส่งก่อนวันลาภายใน 3 วันทำการ สามารถลาได้ไม่เกิน 15 วันต่อปี</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<?php require_once('footer.php') ?>
</body>
</html>