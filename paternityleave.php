<?php
ob_start();
session_start();
ob_end_flush();
require_once('db/db_connect.php');
$sqlStaff = "SELECT * FROM Staff WHERE id =". $_SESSION['id'];
$resultStaff = mysqli_query($con, $sqlStaff);
$resultStaff = mysqli_fetch_array($resultStaff);

$sqlPosition = "SELECT * FROM Position WHERE id =". $resultStaff['position_id'];
$resultPosition = mysqli_query($con, $sqlPosition);
$resultPosition = mysqli_fetch_array($resultPosition);

$sqlDepartment = "SELECT * FROM Department WHERE id =". $resultStaff['department_id'];
$resultDepartment = mysqli_query($con, $sqlDepartment);
$resultDepartment = mysqli_fetch_array($resultDepartment);

$sqlLeaveRequest = "SELECT leave_start_date, leave_end_date, duration, LeaveType_id FROM LeaveRequest WHERE leaverequest_owner_id = ". $_SESSION["id"]." AND status = \"APPROVED\" ORDER BY id DESC LIMIT 1";
$resultLeaveRequest = mysqli_query($con, $sqlLeaveRequest);
if ($resultLeaveRequest->num_rows > 0) {
    $resultLeaveRequest = mysqli_fetch_array($resultLeaveRequest);
    if ($resultLeaveRequest["LeaveType_id"] == 1) 
        $leaveString = "ลาป่วย";
    else if ($resultLeaveRequest["LeaveType_id"] == 4) 
        $leaveString = "ลาคลอดบุตร";
    else if ($resultLeaveRequest["LeaveType_id"] == 2) 
        $leaveString = "ลากิจส่วนตัว";
    else if ($resultLeaveRequest["LeaveType_id"] == 3) 
        $leaveString = "ลาพักผ่อน";
    else if ($resultLeaveRequest["LeaveType_id"] == 5) 
        $leaveString = "ลาดูแลบุตร";
     $startDate = $resultLeaveRequest["leave_start_date"];
     $endDate = $resultLeaveRequest["leave_end_date"];
     $duration = $resultLeaveRequest["duration"];
}
else {
     $leaveString = "-";
     $startDate = "-";
     $endDate = "-";
     $duration = "-";
}

//ปีงบ 1 ตค - 31 กย
if (date('m') >= 1 && date('m') <= 8)
    $year = date('Y')+1;
else
     $year = date('Y');
$sqlLeave = "SELECT * FROM LeaveRequest WHERE leaverequest_owner_id =". $_SESSION['id'] . " AND LeaveType_id = 5 AND status = \"APPROVED\" AND status = \"APPROVED\" AND submit_date BETWEEN \"".$year."-09-01\" AND \"".($year+1)."-08-31\"";

$resultLeave = mysqli_query($con, $sqlLeave);
$useDays = 0;
while ($row = $resultLeave->fetch_assoc()) {
    $useDays += $row["duration"];
}

?>

<script src="js/dayCalScript.js"></script>
<html>
<head>
    <?php
    require_once('header.php');
    ?>
	
	<script>
	var myEvent = window.attachEvent || window.addEventListener;
	var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload'
	
	myEvent(chkevent, function(e){
	var confirmationMessage = ' ';
	(e||window.event).returnValue = confirmationMessage;
	return confirmationMessage;
	});
	</script>
</head>
<body onload = "calParternityDuration();">
<?php require_once('navbar.php') ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>แบบใบลาเพื่อดูแลบุตรและภรรยาหลังคลอด</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <div class="well bs-component" id="form_container">
                <form class="form-horizontal" name="form_data2" method="post" enctype="multipart/form-data" action="submitform.php">
                    <fieldset>
                        <legend>กรุณากรอกข้อมูลให้ครบถ้วนก่อนยืนยันการส่งใบลา</legend>
                        <div class="form-group">
                            <label for="inputCodeEntry" class="col-lg-2 col-lg-offset-7 control-label">รหัสเข้างาน</label>
                            <div class="col-lg-3">
                                <input type="number" class="form-control" name="staffcode" value="<?php echo $resultStaff['staffcode']?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-2 col-lg-offset-7 control-label">วันที่</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control" value="<?php echo date('d'); ?> / <?php echo date('m');?> / <?php echo (date('Y')+543); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-lg-2 control-label">เรื่อง</label>
                            <div class="col-lg-4">
							<input class="form-control" type="text" value="ขอลาเพื่อดูแลบุตรและภรรยาหลังคลอด" disabled>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" name="typename" value = "5">

                        <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label">ข้าพเจ้า ชื่อ</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" value="<?php echo $resultStaff['firstname']?>" disabled>
                            </div>
                            <label for="inputName" class="col-lg-2 control-label">นามสกุล</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" value="<?php echo $resultStaff['lastname']?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label">ตำแหน่ง</label>
                            <div class="col-lg-3">
                                <input class="form-control" type="text" value="<?php echo $resultPosition['positionname']?>" disabled>
                            </div>
                            <label for="inputName" class="col-lg-2 control-label"> สังกัดงาน</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" value="<?php echo $resultPosition['positionname']?>" disabled>
                            </div>
						   </div>
						   
						   <div class="form-group">
						   <label for="wife_name" class="col-lg-2 control-label"> ภรรยา  ชื่อ-นามสกุล</label>
								<div class="col-lg-3">
									<input class="form-control" name="wife_name" type="text" maxlength="" value="" required>
								</div>
						   </div>
						   
						   <div class="form-group">
						   <label for="child_dob" class="col-lg-2 control-label">บุตรคลอดเมื่อวันที่</label>
								<div class="col-lg-2">
										<input type="date" class="form-control" name="child_dob" required>
								</div>
						   </div>
                           <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label"> ได้ลาดูแลบุตรไปแล้ว </label>
                            <div class="col-lg-2">
                                <input class="form-control" name="totalLeaveDay" type="number" maxlength="" size="" value="<?php echo $useDays;?>" disabled>
                            </div>
                            <label for="inputDate" class="col-lg-0 control-label">วันทำการ</label>
                        </div>
						 
                        <div class="form-group">
                            <label for="inputStartDate" class="col-lg-2 control-label">ขอลาตั้งแต่วันที่</label>
                            <div class="col-lg-2">
                                <input type="date" class="form-control" name="leave_start_date" onchange = "calParternityDuration();" required>
                            </div>
                            <label for="inputEndDate" class="col-lg-3 control-label">ถึงวันที่</label>
                            <div class="col-lg-2">
                                <input type="date" class="form-control" name="leave_end_date" required onchange = "calParternityDuration();">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-2 control-label">มีกำหนด</label>
                            <div class="col-lg-1">
                                <input type = "text" class="form-control" name="duration" value = "" required readonly>
                            </div>
                            <label for="inputDate" class="col-lg-0 control-label">วัน</label>
                        </div>
						
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-3 control-label">ข้าพเจ้าได้ลาครั้งสุดท้ายประเภท</label>
                            <div class="col-lg-3">
                                <input class="form-control" id="select" name="lasttypename" required value = "<?php echo $leaveString;?>" disabled><!--nameควรใช้เป็นอะไรดี   -->
                                   
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate" class="col-lg-2 control-label">ครั้งสุดท้ายตั้งแต่วันที่</label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" name="lastStartLeaveDate" required value = "<?php echo $startDate;?>" disabled>
                            </div>
                            <label for="inputDate" class="col-lg-2 control-label">ครั้งสุดท้ายถึงวันที่</label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" name="lastEndLeaveDate" required value = "<?php echo $endDate;?>" disabled>
                            </div>
                            <label for="inputDate" class="col-lg-1 control-label">มีกำหนด</label>
                            <div class="col-lg-1">
                                <input type="text" class="form-control" required value = "<?php echo $duration;?>" disabled>
                            </div>
                            <div class="col-lg-1">
                                <div class="control-label pull-left">วัน</div>
                            </div>
                        </div>
						
						 <div class="form-group">
						<label for="inputPic" class="col-lg-5 control-label" required>แนบไฟล์รูปภาพ-สูติบัตร</label>
						<div class="col-lg-3">
						<input type="file" class="form-control" name="attachment_path1" id="attachment_path1">
						</div>
						</div>
						
						
                        <div class="form-group">
                            <label for="textArea" class="col-lg-3 control-label">ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" rows="5" name="contact_info" id="textAddress" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php') ?>
</body>
</html>