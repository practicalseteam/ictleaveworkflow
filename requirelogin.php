<?php
ob_start();
session_start();
ob_end_flush();
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require_once('header.php');
    ?>
</head>
<body>
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="page-header"></div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Unable to Access this Page</h3>
                </div>
                <div class="panel-body">
                    Your account cannot access this page directly. Please <a href="index.php">Log in</a> again<br>
                    or contact the system administrator to fix it.<br><br>
                    ชื่อผู้ใช้ของคุณไม่สามารถเข้าถึงหน้าเพจนี้ได้ กรุณา <a href="index.php">เข้าสู่ระบบ</a> ใหม่อีกครั้ง<br>
                    หรือติดต่อเจ้าหน้าที่ดูแลระบบ
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php') ?>
</body>
</html>