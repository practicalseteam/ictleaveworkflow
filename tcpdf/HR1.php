<?php


$test = $_GET['test'];

// Include the main TCPDF library (search for installation path).
require_once('tcpdf.php');


//

















// create new PDF document
$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Jannarong Waddthong');
$pdf->SetTitle('HR Report 1');
$pdf->SetSubject('HR Report 1');
$pdf->SetKeywords('HR, Report, leave, ict, project');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
/*
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
*/

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('freeserif', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = "
<!-- EXAMPLE OF CSS STYLE -->
<style>
    h1 {
        color: navy;
       
        font-size: 24pt;
        text-decoration: underline;
    }
    p.first {
        color: #003300;
        ช
        font-size: 12pt;
    }
    p.first span {
        color: #006600;
        font-style: italic;
    }
    p#second {
        color: rgb(00,63,127);
        
        font-size: 12pt;
        text-align: justify;
    }
    p#second > span {
        background-color: #FFFFAA;
    }
    
    div.test {
        color: #CC0000;
        background-color: #FFFF66;
      
        font-size: 10pt;
        border-style: solid solid solid solid;
        border-width: 2px 2px 2px 2px;
        border-color: green #FF00FF blue red;
        text-align: center;
    }
    .lowercase {
        text-transform: lowercase;
    }
    .uppercase {
        text-transform: uppercase;
    }
    .capitalize {
        text-transform: capitalize;
    }
</style>

<h1 class='title'>รายงาน".$test."</h1>


                 <h5>สรุปจำนวนการลาวันนี้ที่ได้รับการอนุมัติจากทุกประเภทการลาและจากทุกสายงานโดยเรียงด้วยรหัสการลาจากน้อยไปหามากตามตัวอักษร</h5>
                <h5>มีทั้งสิ้น    2 รายการ</h5>
                <!-- Table -->
               <table class='table'>
                 <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัสการลา</th>
                        <th>ตำแหน่ง</th>
                        <th>รหัสบุคล</th>
                        <th>รหัสเข้างาน</th>
                        <th>ชื่อ</th>
                        <th>สกุล</th>
                        <th>ประเภทการลา</th>
                        <th>ส่วนงาน</th>
                        <th>สิ้นสุดวันลา</th>

                    </tr>
                </thead>
                <tbody>
                     <tr><td>1</td><td>15</td><td>Deputy Dean</td><td>2</td><td>1235</td><td>Deputy Dean?????</td><td>of ICT</td><td>Paternity Leave</td><td>Administration and Systems Development</td><td>2014-12-20</td></tr><tr><td>2</td><td>18</td><td>Software Analyst</td><td>8</td><td>998877</td><td>Jananrong</td><td>Wadthong</td><td>Vacation Leave</td><td>Board of Administrators</td><td>2014-12-31</td></tr>
                 </tbody>
                </table>



";


// Print text using writeHTMLCell()
//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
