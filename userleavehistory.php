<?php
ob_start();
session_start();
ob_end_flush();
require_once ('db/db_connect.php');
?>
<html>
<head>
    <?php
    require_once('header.php');
    ?>
    <script>
        function showRequest(str) {
            if (str=="") {
                document.getElementById("txtHint").innerHTML="";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET","getrequest.php?q="+str,true);
            xmlhttp.send();
        }
    </script>
</head>
<body>
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="page-header">
        <h2>ระบบการลางานออนไลน์ <small>ICT Leave Request and Approval System</small></h2>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?php require_once('lefttab.php') ?>
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-9">
                    <h3>ประวัติการลางานของคุณ</h3>
                </div>
                <div class="col-lg-3">
                    <h3><select class="form-control" name="requests" onchange="showRequest(this.value)">
                        <option value="0" > ทั้งหมด </option>
                        <option value="1" > ลาป่วย </option>
                        <option value="2" >ลากิจส่วนตัว</option>
                        <option value="3" > ลาพักผ่อน </option>
                        <option value="4" > ลาคลอด </option>
                        <option value="5" > ลาดูแลบุตร </option>
                    </select></h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div id="txtHint">
                        <?
                        $leaveForm = mysqli_query($con,"SELECT * FROM LeaveRequest INNER JOIN LeaveType ON LeaveRequest.LeaveType_id=LeaveType.id
													WHERE LeaveRequest.leaverequest_owner_id='".$_SESSION['id']."'");
                        $countDay = mysqli_query($con,"SELECT SUM(duration) FROM LeaveRequest
													WHERE LeaveRequest.leaverequest_owner_id='".$_SESSION['id']."'");

                        while($r=mysqli_fetch_array($countDay)){
                            echo "จำนวนวันลาทั้งหมด: ".$r['SUM(duration)']." วัน";
                        }
                        ?>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>ลำดับที่</th>
                                <th>ประเภทการลา</th>
                                <th>วันที่ส่งใบลา</th>
                                <th>วันที่เริ่มลา</th>
                                <th>วันสิ้นสุดการลา</th>
                                <th>จำนวนวันลาทั้งหมด</th>
                                <th>เหตุผลที่ลา</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 1;
                            while($row=mysqli_fetch_array($leaveForm)){?>
                                <tr>
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $row['typename'] ?></td>
                                    <td><?php echo $row['submit_date'] ?></td>
                                    <td><?php echo $row['leave_start_date'] ?></td>
                                    <td><?php echo $row['leave_end_date'] ?></td>
                                    <td><?php echo $row['duration'] ?></td>
                                    <td><?php echo $row['reason'] ?></td>
                                </tr>
                                <?php $count++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php') ?>
</body>
</html>