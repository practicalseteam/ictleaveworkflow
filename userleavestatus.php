<?php
ob_start();
session_start();
ob_end_flush();
?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require_once('header.php');
    ?>
</head>
<body>
<?php require_once('navbar.php') ?>

<div class="container">
    <div class="page-header">
        <h2>ระบบการลางานออนไลน์ <small>ICT Leave Request and Approval System</small></h2>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?php require_once('lefttab.php') ?>
        </div>
        <div class="col-lg-9">
            <h3>สถานะใบลาทั้งหมด</h3><br>
            <div class="row">
                <div class="col-md-12">
					<span>
						<?php
							$leaveForm = mysqli_query($con,"SELECT * FROM LeaveRequest INNER JOIN LeaveType ON LeaveRequest.LeaveType_id=LeaveType.id 
															WHERE  LeaveRequest.leaverequest_owner_id='".$_SESSION['id']."'");
						?>
						
					<table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>ประเภทการลา</th>
                            <th>สถานะใบลา</th>
                            <th>รออนุมัติจาก</th>
                            <th>ต้องการลา</th>
                            <th>เหตุผลที่ลา</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 1;
                        while($row=mysqli_fetch_array($leaveForm)){?>

                            <tr>
                                <td><?php echo $count ?></td>
                                <td><?php echo $row['typename'] ?></td>
                                <td><?php echo $row['status'] ?></td>
                                <td><?php echo $row['need_comment_by'] ?></td>
                                <td><?php echo $row['duration'] ?> วัน</td>
                                <td><?php echo $row['reason'] ?></td>
                            </tr>
                            <?php $count++; } ?>
                        </tbody>
					</table>
					</span>
				</div>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php') ?>
</body>
</html>